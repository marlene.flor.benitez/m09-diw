# M09 - Disseny d'interfícies web

## Activitat Inicial M09

Amb l'html i CSS que coneguis, fes una pàgina web estàtica d'una base de dades de pel·licules de cine. Ha de contenir

- header amb imatge, menú, 

- subheader amb imatges de les últimes estrenes

- cos amb una taula de dades de pel·lícules (títol, any, director, actors, durada, gènere, païs, idioma, premis, valoració de la crítica...)

- Footer amb l'avís legal, RGPD, cookies, contacte, faqs

No dono massa detalls, la gracia és que obris segons els teus coneixements previs.

Aquest serà el teu projecte que aniràs "moldejant" en les properes activitats i així veurem l'abans i el després d'aplicar els coneixements d'aquest Mòdul

Entregueu una carpeta comprimida que contingui els CSS i html utilitzats. Si utilitzeu recursos com imatges, vídeos, etc, no els pengeu a l'entrega de la pràctica sino feu que enllaci a un servidor extern amb una ruta absoluta.
