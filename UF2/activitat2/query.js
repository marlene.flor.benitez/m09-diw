 $(document).ready(function() {
    // Exercici 1

    var $alts = $("img[alt]");
    $alts.each(function(i) {
        console.log($(this).attr("alt"));
    });
    
    // Exercici 2

    var $input = $("form input");
    $input.each(function(i){
        $(this).addClass("inputSelected");
    });

    // Exercici 3

    var $item = $("#myList li.current");
    $item.removeClass("current");
    var $next = $item.next();
    $next.addClass("current");

    // Exercici 4

    var $elem = $("#myTableCell").next();
    $elem.css("background-color", "red");

    // Exercici 5

    var $element = $("#slideshow").first();
    $element.addClass("current");
    var $germans = $element.siblings();
    $germans.each(function(i){
        $(this).addClass("disabled");
    });

    // Exercici 6
    var $list = $("#myList");
    for (var $i = 0; $i < 5; $i++){
        $list.append("<li>Elem " + $i + "</li>");
    }

    // Exercici 7

    var $listOdd = $("#myList li:odd");
    $listOdd.remove();

    // Exercici 8

    var $item = $("div.module").last();
    $("<h2>Hola</h2>").appendTo($item);
    $("<p>Ultim module element</p>").appendTo($item);

    // Exercici 9

    var $selected = $("div.module").last().find("select");
    $selected.append("<option>Wednesday</option>");
    $selected.children().last().attr("value", "Wednesday");
    console.log($selected);

    // Exercici 10

    $("div.module").last().after("<div></div>");
    var $last = $("div.module").last().next();
    $last.addClass("module");
    $("img").first().clone().appendTo($last);

});
