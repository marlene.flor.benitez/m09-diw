$(document).ready(function(){
    // 1. Canviar el color de fons de l'element amb id "miElemento”
    $("#miElemento").css("background-color", "lightgreen");
    // 2. Canviar el text de tots els elements amb la classe "elementoImportante”
    $(".elementoImportante").html("Canviar texto");
    // 3. Amagar el tercer paràgraf a la pàgina, independentment de si està dins 
    // d’un altre element
    $("p").eq(2).prop("hidden");
    // 4. Canviar la grandària de la lletra de l'últim element de la llista amb la 
    // classe "listaElementos"
    $(".listaElementos").children().last().css("font-size", "larger");
    // 5. Afegir un borde de color vermell a tots els elements <a> dins d'un 
    // contenidor amb id "menu"
    $("#menu a").css("border", "solid red 1px");
    // 6. Elimina el següent element després del que té l’id=”platanos”. 
    // Fes servir aquest id
    $("#platanos").next().remove();
    // 7. Canviar la grandària de la lletra de tots els elements amb la classe 
    // "importante" que estan dins d'un div amb id "contenedorPrincipal"
    $("#contenedorPrincipal .importante").css("font-size", "small")
});