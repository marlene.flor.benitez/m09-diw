$(document).ready(function(){
    $("#myTable").DataTable({});

    $.ajax({
        url: 'albums.json',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('tbody').empty();
            cargarDatos(data);
        },
        error: function(xhr, status, error) {
            console.error("Error en la solicitud AJAX:", error);    
        }
    });

    function cargarDatos(data) {
        let body = $("tbody");
        data.forEach(function(album) {
            let fila = "<tr>" + 
            "<td>" + album.titol + "</td>" +
            "<td>" + album.any + "</td>" +
            "<td>" + album.autor + "</td>" +
            "<td>" + album.genere + "</td>" +
            "<td><input type='checkbox' class='.micheckbox'></td>" +
            "</tr>";
            body.append(fila);
        });
    };

    $("#myTable").append("<button id='eliminarBtn' class='btn btn-success mt-3 mb-3'>Eliminar files seleccionades</button>")

    $("#eliminarBtn").click(function(){
        let selected = $("#myTable tr td input[type=checkbox]:checked");
        selected.parent().parent().remove();
        //console.log(selected);
    })

});