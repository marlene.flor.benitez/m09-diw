CREATE TABLE peliculas (
    portada VARCHAR(500),
    titol VARCHAR(30),
    fecha VARCHAR(20),
    director VARCHAR(20),
    actors VARCHAR(100),
    durada VARCHAR(20),
    genere VARCHAR(100),
    pais VARCHAR(20),
    premis VARCHAR(60),
    valoracio VARCHAR(10),
    PRIMARY KEY (titol)
);

INSERT INTO peliculas (portada, titol, fecha, director, actors, durada, genere, pais, premis, valoracio) VALUES 
('https://es.web.img3.acsta.net/c_310_420/pictures/17/11/02/13/42/4865983.jpg', 'Wonder', '2017/12/01', 'Stephen Chbosky', 'Julia Roberts, Jacob Tremblay, Owen Wilson', '1h 51m', 'Drama', 'Estats Units', '--', '4.3/5'),
('https://es.web.img2.acsta.net/c_310_420/medias/nmedia/18/89/67/45/20061512.jpg', 'El señor de los anillos', '2001/12/19', 'Peter Jackson', 'Elijah Wood, Sean Astin, Ian McKellen', '2h 58m', 'Fantasia', 'Estats Units', '--', '4.6/5'),
('https://es.web.img3.acsta.net/c_310_420/pictures/23/05/25/13/02/1443257.jpg', 'Elemental', '2023/07/14', 'Peter Sohn', 'Leah Lewis, Mamoudou Athie, Ronnie Del Carmen', '1h 42m', 'Animacio', 'Estats Units', '--', '3.8/5'),
('https://es.web.img3.acsta.net/c_310_420/pictures/16/02/08/16/50/425295.jpg', 'Los otros', '2001/09/07', 'Alejandro Amenábar', 'Nicole Kidman, Elaine Cassidy, Christopher Eccleston', '1h 45m', 'Terror', 'Espanya', '--', '3.2/5'),
('https://pics.filmaffinity.com/le_fabuleux_destin_d_amelie_poulain-848337470-mmed.jpg', 'Amèlie', '2001/10/19', 'Jean-Pierre Jeunet', 'Audrey Tautou, Marhieu Kassovitz, Rufus Magloire', '2h', 'Comedia', 'Francia', '--', '4.8/5'),
('https://pics.filmaffinity.com/las_herederas-820009352-mmed.jpg', 'Las herederas', '2018/04/05', 'Marcelo Martinessi', 'Jana Brun, Margarita Irún, Ana Ivanova', '1h 37m', 'Drama', 'Paraguay', '--', '3.9/5'),
('https://es.web.img2.acsta.net/c_310_420/pictures/23/05/25/13/41/1835431.jpg', 'Oppenheimer', '2023/07/21', 'Christopher Nolan', 'Cillian Murphy, Emily Blunt, Matt Damon', '3h 01m', 'Historico', 'Estats Units', '--', '3.6/5'),
('https://pics.filmaffinity.com/rise_of_the_guardians-746579871-mmed.jpg', 'El Origen de los guardianes', '2012/11/30', 'Peter Ramsey', 'Chris Pine, Alec Baldwin, Isla Fisher, Hugh Jackman', '1h 37m', 'Animacio', 'Estats Units', '--', '3.6/5'),
('https://pics.filmaffinity.com/il_racconto_dei_racconti_the_tale_of_tales-657037591-mmed.jpg', 'El cuento de los cuentos', '2015/05/14', 'Matteo Garrone', 'Salma Hayek, Vincent Cassel, Toby Jones, John C.Reilly', '2h 13m', 'Drama, fantasia, terror', 'Italia', '--', '3.6/5'),
('https://pics.filmaffinity.com/inception-652954101-mmed.jpg', 'Origen', '2010/06/06', 'Christopher Nolan', 'Leonardo DiCaprio, Joseph Gordon-Levit, Elliot Page, Ken Watanabe', '2h 24m', 'Ciencia-ficcio, thriller', 'Estats Units', '--', '3.6/5'),
('https://pics.filmaffinity.com/mortal-463237462-mmed.jpg', 'Mortal', '2023/07/21', 'André Øvredal', 'Nat Wolff, Priyanka Bose, Arthur Hakalahti, Iben Akerlie', '1h 54m', 'Fantastico, ficcion', 'Noruega', '--', '3.6/5'),
('https://pics.filmaffinity.com/doraibu_mai_ka-916845386-mmed.jpg', 'Drive my car', '2023/07/21', 'Ryûsuke Hamaguchi', 'Hideoshi Nisijuma, Toko Miura, Reika Kirishima, Park Yu-rim', '2h 59m', 'Drama', 'Japon', 'Oscar a mejor pelicula extranjera', '3.6/5');
;