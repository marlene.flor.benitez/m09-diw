$(document).ready(function() {
    // Exercici 1

    var $divs = $('div.module');
    console.log('Exercici 1:', $divs);

    //Exercici 2

    var $tercer = $('#myList li:nth-child(3)');
    var $tercer2 = $('#myList li:eq(2)');
    var $tercer3 = $('#myListItem');
    console.log('Exercici 2:', $tercer, $tercer2, $tercer3);

    /*
    Crec que la millor opcio seria la primera donat que estem especificant 
    que sigui el tercer fill del element amb id #myList 
    */

    // Exercici 3

    var $attr = $('label[for="q"]');
    console.log('Exercici 3: ', $attr);

    // Exercici 4

    var $hidden = ($("*:not(:visible)").length);
    $hidden = ($(":hidden")).length;
    console.log('Exercici 4: ', $hidden);

    // Exercici 5

    var $images = $("[alt]");
    $images = ($("img[alt]")).length;
    console.log('Exercici 5: ', $images);

    // Exercici 6

    var $even = ($("tr:even"));
    console.log('Exercici 5: ', $even);

});