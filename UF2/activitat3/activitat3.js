$(document).ready(function() {
    // Exerici 1

        $("#filmTable tr:nth-child(3)").remove();

    // Exercici 2

        $("#filmTable").append("<button id='fila'>Agregar fila</button>")
        $("#fila").click(function(){
            $("#filmTable").append("<tr><td><img src='https://pics.filmaffinity.com/la_la_land-262021831-mmed.jpg'></img></td><td>2017/07/13</td><td><p><strong>Títol:</strong> La la land</p><p><strong>Director: </strong> Damien Chazelle</p><p><strong>Acorts: </strong> Emma Stone, Ryan Gosling, John Legend, Rosemarie Dewitt</p><p><strong>Durada: </strong> 2h 7min</p><p><strong>Gènere: </strong> Musical, romance </p><p><strong>Pais: </strong> Estats Units </p><p><strong>Idioma:</strong> Anglès</p><p><strong>Premis:</strong> 6 Premios Oscar</p><p><strong>Valoració:</strong> 7.5/10 </p></td><td><input type='checkbox' class='.micheckbox'></td></tr>")
        })
    
    // Exercici 3

        const rows = $("#filmTable tbody tr");
        rows.each(function(i){
            let checked = $("#filmTable tbody tr:nth-child(" + i + ")");
            checked.append("<td><input type='checkbox' class='.micheckbox'></td>");
        });

        $("#filmTable").append("<button id='esborrarFila'>Esborrar fila</button>");
        // USAR WHILE SI HE DE MODIFICAR NUMERO
        $("#esborrarFila").click(function(){
            for (let i = 0; i < rows.length; i++){
            let box = $("#filmTable tbody tr:nth-child(" + i + ") input");
            if (box.prop("checked")){
                console.log(box.prop("checked"));
                box.parent().parent().remove();
                i--;
                } 
            }
        })

    // Exercici 4

        var table = $("#filmTable");
        table.mouseenter(function(){
            table.removeClass("table-dark");
        });

        table.mouseleave(function(){
            table.addClass("table-dark");
        })
    

    // Exercici 5

        var img = $("#filmTable img");
        console.log(img)
        img.each(function(i){
            console.log(i)

           $(this).hover(function(){
               $(this).fadeTo("slow", 0.15);} ,
               function(){
               $(this).fadeTo("slow", 1); }
            )
        });

});
