
$(document).ready(function (){

    $.ajax({
        url: 'carregarDirector.php',
        method: 'GET',
        dataType: 'json',
        success: function(data) {
            console.log("data");
            console.log(data);
            $('#directorFiltrar').empty().append('<option value="">Selecciona un director</option>');
            for (var i = 0; i < data.length; i++) {
                $('#directorFiltrar').append('<option value="'+ data[i] + '">' + data[i] + '</option>');
            }

        },
        error: function(xhr, status, error, data) {
            console.log(data);
            console.error("Error al eliminar registros:", error);
        }
    });

    $('#filmTable').DataTable( {
        pagingType: 'full_numbers',
        dom: 'Bfrtip',
        buttons: [ 
            { extend: 'copy', text: 'Copiar', exportOptions: {modifier: {page: 'current'}}},
            { extend: 'excel', text: 'Exportar a Excel', customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                $('row c[r^="C"]', sheet).attr( 's', '2' );
            } }
        ],
    } );

    var filaDirector = $('table#example tbody tr:eq(2)');
    filaDirector.remove();
    var directorSelect = $('directorFiltrar');
    // Manejar el cambio en el selector de directores
    directorSelect.on('change',function() {
        var director = $('#directorFiltrar').val();
        if (director === "") {
            $('#selectGenero').empty().append('<option value="">Selecciona un género</option>').prop('disabled', true);
         }
        else {
            $.ajax({
                url: 'carregarGeneres.php',
                method: 'GET',
                dataType: 'json',
                data: { director: director },
                success: function(data) {
                    var generoSelect = $('#selectGenero');
                    generoSelect.empty().prop('disabled', false);
                    generoSelect.append('<option value="">Selecciona un género</option>');
                    $(data).each(function(index, genero) {
                        generoSelect.append('<option value="' + genero + '">' + genero + '</option>');
                    });
                    var genero = $('#selectGenero').val();
                    table.column(3).search(director);
                    table.column(4).search(genero);
                    table.draw();
                }
            });
        }
    });

    const rows = $("#filmTable tbody tr");
        rows.each(function(i){
            let checked = $("#filmTable tbody tr:nth-child(" + i + ")");
            checked.append("<td><input type='checkbox' class='.micheckbox'></td>");
        });

    // Quan cliquem el boto esborrem les dades de la taula i carreguem les del json

    $("#carregarJsonBtn").click(function(){
        $.ajax({
            url: 'peliculas.json',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                $('tbody').empty();
                cargarDatos(data);
            },
            error: function(xhr, status, error) {
                console.error("Error en la solicitud AJAX:", error);    
            }
        });
    })

    function cargarDatos(data) {
        var tableBody = $('tbody');
        data.forEach(function (data) {
        var fila = '<tr>' +
        '<td><img class="img-fluid" src="' + data.portada + '" alt="Imagen"></td>' +
        '<td>' + data.titol + '</td>' +
        '<td>' + data.fecha + '</td>' +
        '<td>' + data.director + '</td>' +
        '<td>' + data.actors + '</td>' +
        '<td>' + data.durada + '</td>' +
        '<td>' + data.genere + '</td>' +
        '<td>' + data.pais + '</td>' +
        '<td>' + data.premis + '</td>' +
        '<td>' + data.valoracio + '</td>' +
        '</tr>';
        tableBody.append(fila);
        })
    };

    // botó per carregar dades des de la BBDD
    
    $("#carregarDbBtn").on('click', function() {
        $.ajax({
            url: "cargar-db.php", // Cambia esto con la ruta correcta a tu script PHP
            type: "GET",
            success: function(data) {
              // Reemplaza el contenido de la tabla con los datos actualizados
              $("#filmTable tbody").html(data);
            },
            error: function() {
              alert("Error al obtener datos de la base de datos");
            }
          });
    });

    // Afegir nous registres

    $("#nuevaPeliculaBtn").on("click", function() {
		window.open("./formulari.html");
	});


    // Eliminar pelicules seleccionades
    $('#eliminarFilasBtn').on('click', function() {
        // Array para almacenar los ID de las películas a eliminar
        var peliculasAEliminar = [];
        // Iterar sobre las filas de la tabla
        $("td input[type=checkbox]:checked").parent().each(function() {
            peliculasAEliminar.push($(this).siblings().eq(1).html());
            //console.log($(this).siblings().eq(1).html());
        });

        // Enviar la solicitud AJAX para eliminar las películas seleccionadas
        $.ajax({
            url: "eliminarPeliculas.php",
            type: "POST",
            data: { peliculas: peliculasAEliminar },
            success: function(response) {
                $("#carregarDbBtn").click();
                console.log(response);
            },
            error: function(xhr, status, error) {
                console.error("Error al eliminar registros:", error);
            }
        });

    });

});


