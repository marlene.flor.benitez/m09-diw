<?php
$servername = "cine-ajax-db-1:3306";
$username = "mysql";
$password = "test";
$dbname = "dbname";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully";

$sql = "SELECT DISTINCT director FROM peliculas WHERE director != ''";
$result = $conn->query($sql);

$directores = array();

if ($result) {
    // Si se encuentran resultados, los almacenamos en un array
    while ($row = $result->fetch_assoc()) {
        $directores[] = $row;
    }
}

// Devolver los resultados como JSON
echo json_encode($directores);


$conn->close();

?>