<?php
$servername = "cine-ajax-db-1:3306";
$username = "mysql";
$password = "test";
$dbname = "dbname";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully";

$sql = "SELECT * FROM peliculas;";
$result = $conn->query($sql);

while ($row = $result->fetch_assoc()) {
  echo '<tr>' .
  '<td><img class="img-fluid" src="' . $row["portada"] . '" alt="Imagen"></td>' .
  '<td>' . $row["titol"] . '</td>' .
  '<td>' . $row["fecha"] . '</td>' .
  '<td>' . $row["director"] . '</td>' .
  '<td>' . $row["actors"] . '</td>' .
  '<td>' . $row["durada"] . '</td>' .
  '<td>' . $row["genere"] . '</td>' .
  '<td>' . $row["pais"] . '</td>' .
  '<td>' . $row["premis"] . '</td>' .
  '<td>' . $row["valoracio"] . '</td>' .
  '</tr>';
}
?>