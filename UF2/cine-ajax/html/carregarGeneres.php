<?php
$servername = "cine-ajax-db-1:3306";
$username = "mysql";
$password = "test";
$dbname = "dbname";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
echo "Connected successfully";

$director = $_POST("director");
$sql = "SELECT DISTINCT genere FROM peliculas WHERE director =" . $director;
$result = $conn->query($sql);
$generes = array();

if ($result) {
  // Si se encuentran resultados, los almacenamos en un array
  while ($row = $result->fetch_assoc()) {
      $generes[] = $row;
  }
}

echo json_encode($generes);

$conn->close();

?>