$(document).ready(function() {
	$('#formNuevaPelicula').submit(function(event){
		//evitar que se recargue el formulario al enviar
		event.preventDefault();
		/*
		Esto no sirve ya que el .val no sirve para la imagen
		var peliData ={
			imagen: $("#imagen").val(),
			nombre: $("#nombre").val(),
			fecha: $("#fecha").val(),
			puntuacion: $("#puntuacion").val()
		};*/

		//Crear un objecto form data  para obtener los datos del formularo facilmente
		//el [0] para acceder al primer elemento de un jquery
		var formData = new FormData($('#formNuevaPelicula')[0]);
		$.ajax({
			type: "POST",
			url: "afegirPelicula.php",
			/*
			//Con esta forma podemos pasar los datos dekl form pero la imagen no.
			data: {
				imagen_url: peliData.imagen,
				nombre: peliData.nombre,
				fecha: peliData.fecha,
				puntuacion: peliData.puntuacion
			},*/
			data: formData,
			dataType: 'json',
			//contentType y processData son necesarios cuando usas el formData
			//contentType al ponerlo false evita que se estabelzca automáticamente el encabezado Content-Type y asi permitimos el uso de multipart/form-data
			contentType: false,
			//processData evita que jQuery convierta automáticamente los datos en una cadena de consulta (query string) 
            processData: false,
			success: function (respuesta) {
				if (respuesta.success) {
					alert("insertado con exito");
				} else {
					alert("error al insertar" + respuesta.message);
				}
		},
		//error:function (xhr, status, error){
		//	console.error(error);
		//}
	});
});
});
